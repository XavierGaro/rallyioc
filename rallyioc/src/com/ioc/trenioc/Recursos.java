package com.ioc.trenioc;

import com.ioc.motor.Musica;
import com.ioc.motor.Pixmap;
import com.ioc.motor.So;

public class Recursos {
	// Gràfics del joc
	public static Pixmap logo;
	public static Pixmap menuPrincipal;
	public static Pixmap botons;
	public static Pixmap ajuda1;
	public static Pixmap ajuda2;
	public static Pixmap ajuda3;
	public static Pixmap ajuda4; // Afegida pantalla d'ajuda 4
	public static Pixmap numeros;
	public static Pixmap numerosVermell;
	public static Pixmap preparat;
	public static Pixmap pausa;
	public static Pixmap gameOver;
	public static Pixmap trenAdalt;
	public static Pixmap trenEsquerra;
	public static Pixmap trenAbaix;
	public static Pixmap trenDreta;
	public static Pixmap obstacle;
	public static Pixmap carbo;
	
	// Afegits els vagons
	public static Pixmap vagoAdalt;
	public static Pixmap vagoEsquerra;
	public static Pixmap vagoAbaix;
	public static Pixmap vagoDreta;

	// Afegit fons del títol i quadres
	public static Pixmap fonsTitol;
	public static Pixmap quadreAjuda;
	public static Pixmap quadreRecord;
	public static Pixmap quadreGui;
	
	// Afegit missatge de completat
	public static Pixmap completat;

	// Sons del joc
	public static So soClick;
	public static So soCarbo;
	public static So soCrash;

	// Afegit so de nivell complet
	public static So soComplet;

	public static Musica soMusica;

	// Mapas dels nivells
	public static Pixmap[] fons;


}