package com.ioc.trenioc;

public class Obstacle extends Element {
	public Obstacle(int x, int y) {
		super(x, y, ELEMENT_OBSTACLE);
	}
}