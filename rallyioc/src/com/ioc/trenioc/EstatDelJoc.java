package com.ioc.trenioc;

public class EstatDelJoc {

	public static final int Preparat = 0;
	public static final int Jugant = 1;
	public static final int Pausa = 2;
	public static final int GameOver = 3;
	public static final int NivellCompletat = 4; // Estat al que s'arriba al completar el nivell
	
}
